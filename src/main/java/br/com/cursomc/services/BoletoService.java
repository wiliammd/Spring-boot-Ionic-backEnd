package br.com.cursomc.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import br.com.cursomc.domain.PagamentoBoleto;
@Service
public class BoletoService {
	public void preencherPagamentoComBoleto(PagamentoBoleto pgt, Date instanteDoPedido) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(instanteDoPedido);
		cal.add(Calendar.DAY_OF_MONTH, 7);
		pgt.setDataVencimento(cal.getTime());
	}
}
